#ifndef _H_VIRTUAL_INTERRUPT_H_
#define _H_VIRTUAL_INTERRUPT_H_

#include <cstdint>
#include <functional>
#include <vector>

namespace vint {
    class Interrupt
    {
    public:
    	typedef std::function<uint32_t(void)> GetTickCountFn;
		typedef std::function<void(void)> TockFn;
		struct Entry {
			uint32_t period;
			uint32_t tick;
			TockFn tock;
		};
		typedef std::vector<Entry>::iterator Handle;
        Interrupt() : get_tick_count_(nullptr) {}
        ~Interrupt(){}
		void set_get_tick_count(GetTickCountFn func) {get_tick_count_ = func;}
		uint32_t get_tick_count() {
			if (get_tick_count_) {
				return get_tick_count_();
			}
			return 0;
		}
		Handle set_interrupt(uint32_t period, TockFn func) {
			Entry entry;
			entry.tick = get_tick_count();
			entry.period = period;
			entry.tock = func;
			table_.push_back(entry);
			return table_.end() - 1;
		}
		void remove(const Handle &handle) {
			table_.erase(handle);
		}
		void run_loop() {
			uint32_t tick = get_tick_count();
			for (auto &entry : table_) {
				if ((tick - entry.tick) >= entry.period) {
					entry.tick = tick;
					entry.tock();
				}
			}
		}
	private:
		GetTickCountFn get_tick_count_;
		std::vector<Entry> table_;
    };
}

#endif
